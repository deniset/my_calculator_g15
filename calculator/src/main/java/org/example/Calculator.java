package org.example;

public class Calculator {

    static public double calculate(double num1, String operator, double num2) {
        double ret;
        try {
            ret = switch (operator) {
                case "+" -> _add(num1, num2);
                case "*" -> _multiply(num1, num2);
                case "-" -> _subtract(num1, num2);
                case "/" -> {
                    if (num2 == 0) {
                        throw new ArithmeticException("Division by zero is not allowed.");
                    }
                    yield _divide(num1, num2);
                }
                case "^" -> _square(num1);
                default -> throw new UnsupportedOperationException("Unsupported operator: " + operator);
            };
        } catch (ArithmeticException e) {
            System.out.println(e.getMessage());
            return Double.NaN;
        } catch (UnsupportedOperationException e) {
            System.out.println(e.getMessage());
            return Double.NaN;
        }
        return ret;
    }


    /**
     * Adds two double numbers
     * @param num1
     * @param num2
     * @return num1 + num2
     */
    static double _add(double num1, double num2) {
        return num1 + num2;
    }

    /**
     * Subtracts two double numbers
     * @param num1
     * @param num2
     * @return num1 - num2
     */
    static double _subtract(double num1, double num2) {
        return num1 - num2;
    }

    /**
     * Divides two double numbers
     * @param num1
     * @param num2
     * @return num1 / num2
     * @throws ArithmeticException If num2 is zero
     */
    static double _divide(double num1, double num2) throws ArithmeticException {
        double result = num1 / num2;
        if(num2==0)
            System.out.println("Impossivel dividir por zero");

        if(result==0){
            return Math.abs(result);
        }
        return result;

    }

    /**
     * Multiplies two double numbers
     * @param num1
     * @param num2
     * @return num1 * num2
     */
    static double _multiply(double num1, double num2) {
        double result =num1 * num2;
        if(result==0){
            return Math.abs(result);
        }
        return result;
    }

    /**
     * Calculates the power of two of one number
     * @param num1
     * @return num1 * num1
     */
    static public double _square(double num1) {
        return num1 * num1;
    }


}
