package org.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        while (true) {
            double num1, num2, result;
            String op;

            System.out.println("\n");
            System.out.println(" MY PROJECT - PERSONAL CALCULATOR");
            System.out.println(" Denise do Carmo (G15)");
            System.out.println(" =======================");
            System.out.println(" ATTENTION TO THE INPUT OPERATOR: ");
            System.out.println(" Use - ^ - to calculate square, x^2");
            System.out.println(" =======================\n");





            System.out.println("num1 : ");
            if (s.hasNextDouble()) {
                num1 = s.nextDouble();
            } else {
                return;
            }

            System.out.println("operator : ");
            op = s.next();

            if (op.equals("^")) {
                result = Calculator._square(num1);
                System.out.println(num1 + "^2 = " + result);
            } else {
                System.out.println("num2 : ");
                if (s.hasNextDouble()) {
                    num2 = s.nextDouble();
                } else {
                    return;
                }
                if(op.equals("/")&& num2==0){
                    System.out.println("Impossivel dividir por zero");
                }else{
                    result = Calculator.calculate(num1, op, num2);
                    System.out.println(num1 + " " + op + " " + num2 + " = " + result);
                }
            }
        }
    }
}