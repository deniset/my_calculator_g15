package org.example;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTests {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void addTest() {
        assertEquals(0.0, Calculator._add(0, 0));
        assertEquals(6.0, Calculator._add(2, 4));
        assertEquals(-2.0, Calculator._add(0, -2));
        assertEquals(1.0, Calculator._add(6, -5));
        assertEquals(-1, Calculator._add(-6, 5));
        assertEquals(-10.0, Calculator._add(-5, -5));

    }

    @Test
    void subtractTest() {
        assertEquals(0.0, Calculator._subtract(0, 0));
        assertEquals(-2.0, Calculator._subtract(2, 4));
        assertEquals(2.0, Calculator._subtract(0, -2));
        assertEquals(10.3, Calculator._subtract(5.3, -5));
        assertEquals(-2.1, Calculator._subtract(-1, 1.1));
        assertEquals(0.0, Calculator._subtract(-5, -5));

    }

    @Test
    void divideTest() {
        assertEquals(Double.NaN, Calculator._divide(0, 0));
        assertEquals(0.5, Calculator._divide(2, 4));
        assertEquals(0.0, Calculator._divide(0, -2));
        assertEquals(-1.06, Calculator._divide(5.3, -5));
        assertEquals(-0.625, Calculator._divide(-5, 8));
        assertEquals(1.0, Calculator._divide(-5, -5));
    }

    @Test
    void multiplyTest() {
        assertEquals(8.0, Calculator._multiply(2, 4));
        assertEquals(0.0, Calculator._multiply(0, -2));
        assertEquals(-26.5, Calculator._multiply(5.3, -5));
        assertEquals(-1.1, Calculator._multiply(-1, 1.1));
        assertEquals(25.0, Calculator._multiply(-5, -5));
    }

    @Test
    void squareTest() {
        assertEquals(4.0, Calculator._square(2));
        assertEquals(1.44, Calculator._square(1.2));
        assertEquals(4, Calculator._square(-2));




    }
}
